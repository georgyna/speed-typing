const activeUsers = {};
const joinedRooms = [];

export default io => {
  io.of("/game").on("connection", socket => {
    const username = socket.handshake.query.username;
    if (Object.values(activeUsers).includes(username)) {
      socket.emit("USER_EXISTS", true);
    } else {
      activeUsers[socket.id] = username;
    }

    socket.on("JOIN_ROOM", roomName => {
      if (joinedRooms.includes(username)) {
        socket.emit("JOIN_ROOM_ERROR", roomName);
      } else {
        socket.join(roomName, () => {
          io.of("/game").to(roomName).emit("JOIN_ROOM_DONE", { roomName });
        });
        joinedRooms.push(roomName);
      }
    });

    socket.on("GET_ROOM_USERS", roomName => {
      io.of("/game").in(roomName).clients((err , clients) => {
        const clientNames = clients.map(cliendId => activeUsers[cliendId]);
        io.of("/game").to(roomName).emit("ROOM_USERS", clientNames);
      });
    });

    socket.on("disconnect", () => {
      delete activeUsers[socket.id];
    });
  });
};
