import { createElement } from "./helper.mjs";

const gameContainer = document.getElementById("users-progress");

export const openRoom = name => {
  const roomHeader = document.getElementById("room-name");
  roomHeader.innerHTML = name;
};

export const displayRoomUsers = users => {
  console.log(users);
  const userList = users.map(createUser);
  gameContainer.append(...userList);
};

const createUser = user => {
  const userEl = createElement({ tag: 'div', className: 'flex' });
  userEl.innerHTML = user;
  return userEl;
}