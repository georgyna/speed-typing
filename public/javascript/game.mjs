import { createElement } from "./helper.mjs";
import { removeClass, addClass } from "./helper.mjs";
import { openRoom, displayRoomUsers } from "./room.mjs";

const username = sessionStorage.getItem("username");
const socket = io("http://localhost:3002/game", { query: { username } });

const roomsContainer = document.getElementById("rooms-cards");

if (!username) {
  window.location.replace("/login");
}

const showUserError = show => {
  if (show) {
    window.location.replace("/login");
    alert("This name is already taken!");
    sessionStorage.clear(); 
  }
};

const showRoomError = roomName => {
  alert(`Room with name ${roomName} already exists!`);
}

const createJoinButton = roomName => {
  const joinButton = createElement({ tagName: "div", className: "ui bottom attached button" });
  const onJoinRoom = () => {
    const roomName = prompt("Please, enter name of the room");
    socket.emit("JOIN_ROOM", roomName);
  };
  joinButton.addEventListener("click", onJoinRoom);
  joinButton.innerText = "JOIN";
  return joinButton;
};

const createRoom = roomName => {
  const roomCard = createElement({ tagName: "div", className: "card", attributes: { id: "roomName" }});
  const cardContent = createElement({ tagName: "div", className: "content" });
  const cardHeader = createElement({ tagName: "div", className: "header" });
  cardHeader.innerText = `Room ${roomName}`;
  const cardDescription = createElement({ tagName: "div", className: "description" });
  cardDescription.innerText = "Users connected 0";
  const joinButton = createJoinButton(roomName);
  cardContent.append(cardHeader, cardDescription);
  roomCard.append(cardContent, joinButton);
  return roomCard;
};

const createRoomsList = () => {
  const allRooms = [1, 2, 3, 4].map(createRoom);
  roomsContainer.append(...allRooms);
};

export const getUsers = () => {
  socket.emit("GET_ROOM_USERS", roomName);
}

const joinRoomDone = ({ roomName }) => {
  const roomsPage = document.getElementById("rooms-page");
  addClass(roomsPage, "display-none");
  const gamePage = document.getElementById("game-page");
  removeClass(gamePage, "display-none");
  openRoom(roomName, socket);
  getUsers();
};

createRoomsList();
socket.on("USER_EXISTS", showUserError);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("JOIN_ROOM_ERROR", showRoomError);
socket.on("ROOM_USERS", displayRoomUsers);